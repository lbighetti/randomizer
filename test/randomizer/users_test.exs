defmodule Randomizer.UsersTest do
  use Randomizer.DataCase

  alias Randomizer.Users
  alias Randomizer.Users.User

  describe "users" do
    test "list_users/0 returns all users" do
      assert [
               %User{
                 points: points,
                 inserted_at: %DateTime{},
                 updated_at: %DateTime{}
               },
               %User{
                 points: points2,
                 inserted_at: %DateTime{},
                 updated_at: %DateTime{}
               }
             ] = Users.list_users()

      assert is_integer(points)
      assert is_integer(points2)
    end

    test "update_user/2 with valid data updates the user" do
      user_before = Repo.get(User, 1)
      assert Users.update_users() == :ok
      user_after = Repo.get(User, 1)
      # This will flake 1 in a 100 times, when the new value is coincidentally the same as the old
      # however I decided to make this tradeoff in favour of the reliability of the test
      assert(
        user_before.points != user_after.points,
        "This could be false negative, might be a coincidence, run again to be sure."
      )

      assert DateTime.diff(user_after.updated_at, user_before.updated_at) != 0
    end
  end
end
