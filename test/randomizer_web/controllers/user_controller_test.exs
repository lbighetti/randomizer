defmodule RandomizerWeb.UserControllerTest do
  use RandomizerWeb.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, ~p"/")
      response = json_response(conn, 200)
      assert [%{"points" => _points}, %{"points" => _points2}] = response["users"]
      # FIXME:
      # assert response["timestamp"]
    end
  end
end
