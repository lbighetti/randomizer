now = DateTime.now!("Etc/UTC")

users =
  Enum.map(1..1_000_000, fn _ ->
    %{points: 0, inserted_at: now, updated_at: now}
  end)

# 21_000 was determined empirically, any more and we exceed postgres maximum parameters
users
|> Enum.chunk_every(21_000)
|> Enum.each(fn users_batch ->
  Randomizer.Repo.insert_all(Randomizer.Users.User, users_batch)
end)
