now = DateTime.now!("Etc/UTC")

# putting a value different than 0 here to make it easier to test
users =
  Enum.map(1..1_000, fn _ ->
    %{points: Enum.random(1..100), inserted_at: now, updated_at: now}
  end)

Randomizer.Repo.insert_all(Randomizer.Users.User, users)
