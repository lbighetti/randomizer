defmodule Randomizer.Repo.Migrations.CreateIndexForPointsField do
  use Ecto.Migration
  @disable_ddl_transaction true
  @disable_migration_lock true

  def change do
    create index("users", [:points],
             concurrently: true,
             name: :users_points
           )
  end
end
