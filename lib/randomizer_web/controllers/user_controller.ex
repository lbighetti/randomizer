defmodule RandomizerWeb.UserController do
  use RandomizerWeb, :controller

  alias Randomizer.UserServer

  action_fallback RandomizerWeb.FallbackController

  def index(conn, _params) do
    {users, timestamp} = UserServer.fetch_users()
    render(conn, :index, %{users: users, timestamp: timestamp})
  end
end
