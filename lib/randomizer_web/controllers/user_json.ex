defmodule RandomizerWeb.UserJSON do
  alias Randomizer.Users.User

  @doc """
  Renders a list of users and the timestamp of the last query.
  """
  def index(%{users: users, timestamp: timestamp}) do
    %{users: for(user <- users, do: data(user))}
    |> Map.merge(%{timestamp: timestamp})
  end

  defp data(%User{} = user) do
    %{
      id: user.id,
      points: user.points
    }
  end
end
