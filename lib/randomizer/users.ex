defmodule Randomizer.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Randomizer.Repo

  alias Randomizer.Users.User

  @doc """
  Returns 2 users who have more points than the passed in parameter.
  """
  def list_users(points \\ -1) do
    query =
      from u in User,
        where: u.points > ^points,
        select: struct(u, [:id, :points, :updated_at, :inserted_at]),
        limit: 2

    Repo.all(query)
  end

  @doc """
  Updates all users points with new random points.
  """
  def update_users() do
    from(p in User, update: [set: [points: fragment("floor(random()*100)"), updated_at: fragment("now()")]])
    |> Repo.update_all([], timeout: 55_000)
    :ok
  end
end
