defmodule Randomizer.Repo do
  use Ecto.Repo,
    otp_app: :randomizer,
    adapter: Ecto.Adapters.Postgres
end
