defmodule Randomizer.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      RandomizerWeb.Telemetry,
      Randomizer.Repo,
      {Phoenix.PubSub, name: Randomizer.PubSub},
      RandomizerWeb.Endpoint,
      {Randomizer.UserServer, Enum.random(1..100)}
    ]

    opts = [strategy: :one_for_one, name: Randomizer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def config_change(changed, _new, removed) do
    RandomizerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
