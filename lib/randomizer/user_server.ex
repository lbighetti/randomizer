defmodule Randomizer.UserServer do
  # TODO: implement test for this module
  use GenServer

  alias Randomizer.Users

  @time_in_milliseconds Application.compile_env(:randomizer, :sync_time_in_milliseconds)

  # Client

  def start_link(default) when is_integer(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def fetch_users() do
    GenServer.call(__MODULE__, :fetch_users)
  end

  # Callbacks

  @impl true
  def init(min_number) do
    schedule_work()
    {:ok, {min_number, nil}}
  end

  @impl true
  def handle_call(:fetch_users, _from, {min_number, timestamp}) do
    users = Users.list_users(min_number)
    new_timestamp = DateTime.now!("Etc/UTC")
    {:reply, {users, timestamp}, {min_number, new_timestamp}}
  end

  @impl true
  def handle_info(:update, {_min_number, timestamp}) do
    schedule_work()
    # TODO: should this be supervised?
    # FIXME: this makes it non-blocking but causes other issues, deadlocking on the update
    # I think I'll end up having to make the update just faster than what it is now
    Task.start(fn -> :ok = Users.update_users() end)
    min_number = Enum.random(1..100)
    {:noreply, {min_number, timestamp}}
  end

  defp schedule_work do
    Process.send_after(self(), :update, @time_in_milliseconds)
  end
end
