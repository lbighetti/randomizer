# Randomizer

![randomizer logo](https://hopeful-bose-df991f.netlify.com/plan.png)
> An user handler with random points generator and timestamps.

This is project implements the specs specified in https://remotecom.notion.site/Backend-code-exercise-d7df215ccb6f4d87a3ef865506763d50.
## Setup
To start your Phoenix server:

  * Run `asdf install` for grabbing the correct elixir and erlang versions
    * If you don't like `asdf` you can use your preferred method, current elixir/erlang versions are:
      * elixir 1.14.1
      * erlang 25.0.4 
    * If you would like to use `asdf` but don't have it installed, check [here for instructions](https://asdf-vm.com/guide/getting-started.html)
  * Run `mix setup` to install and setup dependencies, runs seeds and database migrations
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`
  * Server will now run on `http://localhost:4000`

Postgres is using the user `postgres` with password `postgres`. This is not always the default setup in a postgres installation, so adjust as necessary in the config files.

## Overview

The project is implemented as described in the specifications. In short,
there is one single api endpoint on the root, it can be called with a get request to it, which will return 2 users containing a number of points above a certain threshold stored internally.

To call the webserver:
- `curl http://localhost:4000/`

And the response will look like so:
```json
{
    "timestamp": "2023-03-01T16:29:44.854157Z",
    "users": [
        {
            "id": 106,
            "points": 80
        },
        {
            "id": 83,
            "points": 9
        }
    ]
}
```

The actual threshold `min_number` is store internally and it's not displayed on the response, as specified.
However it is worth noting that this `min_number` threshold is refreshed every minute with a random number from 1 to 100, and so are all the `users.points`.

The `timestamp` on the response is the time of the previous call to the endpoint, this can be visulised by
making consecutive calls and taking note of the changing `timestamp`.

## Other Notes

Project was generated using the following command:
- `mix phx.new randomizer --no-assets --no-html --no-live --no-mailer --no-gettext`
  - This removes unneeded frontend structure, liveview, mailer and gettext (translations).

User resource was created using
'mix phx.gen.json Users User users points:integer' and cleaned up
and adjusted to fit requirements.

Indexed `users` table on `points` field to speed up querying it by this field.

Made a small trickery for speeding up the update, where I group the user_id based on the new points value and bulk update them. This is explained a bit better on the function docs.